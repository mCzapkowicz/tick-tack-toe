import React from 'react';
import './App.scss';
import Board from "./components/Board";


function App () {
    return (
        <div>
            <h1 className="title">Tick Tack Toe</h1>
            <div className="App">
                <Board/>
            </div>
        </div>

    );
}

export default App;
