import React from 'react';
// single box
export default function Box (props) {
    return (
        <div className="box">{props.boxValue}</div>
    )
}
